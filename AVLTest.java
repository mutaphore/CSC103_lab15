import java.util.*;

public class AVLTest
{
   public static void main(String[] args)
   {
      BasicAVL tree = new BasicAVL();
      
      System.out.println("Choose one of the following operations:");
      System.out.println("    - add/insert (enter the letter a)");
      System.out.println("    - print (enter the letter p)");
      System.out.println("    - quit (enter the letter q)");
      
      Scanner input = new Scanner(System.in);
      
      char choice = 'z';
      int temp;
      
      while(choice != 'q')
      {
         System.out.println("Enter choice: ");
         
         choice = input.nextLine().charAt(0);
         
         switch(choice)
         {
            case 'a':
            System.out.print("Enter a number to be added: ");
            temp = input.nextInt();
            input.nextLine();
            tree.insert(temp);
            System.out.println(temp + " has been inserted.");
            break;
            
            case 'p':
            tree.print();
            break;
            
            case 'q':
            System.out.println("Quitting.");
            break;
            
            default:
            System.out.println("Invalid choice.");
            break;
         }
      }
   }
}